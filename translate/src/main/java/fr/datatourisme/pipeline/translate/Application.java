/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.pipeline.translate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Import;


/**
 * Main Application
 */
@SpringBootConfiguration
@Import({ Configuration.class })
public class Application implements CommandLineRunner
{
    @Autowired
    SimpleMessageListenerContainer listenerContainer;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... args) {
        listenerContainer.start();
        System.out.println("[*] Waiting for triples. To exit press CTRL+C");

        while(true) {
            try {
                Thread.sleep(1000 * 120);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
