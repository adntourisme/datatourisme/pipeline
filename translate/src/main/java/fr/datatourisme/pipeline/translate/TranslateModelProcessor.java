/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.pipeline.translate;

import fr.datatourisme.pipeline.utils.JenaUtils;
import fr.datatourisme.pipeline.ModelProcessor;
import fr.datatourisme.pipeline.vocabulary.Datatourisme;
import fr.datatourisme.pipeline.vocabulary.DatatourismeData;
import fr.datatourisme.pipeline.vocabulary.DatatourismeMetadata;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.*;
import org.apache.jena.util.MonitorModel;
import org.apache.jena.util.ResourceUtils;
import org.apache.jena.vocabulary.DC_11;
import org.apache.jena.vocabulary.RDF;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class TranslateModelProcessor implements ModelProcessor {
    private static final Logger LOG = LoggerFactory.getLogger(TranslateModelProcessor.class);
    private static final Language SRC_LANGUAGE = Language.FR;
    private final DeeplTranslateService translateService;
    private final ApplicationProperties applicationProperties;

    public TranslateModelProcessor(DeeplTranslateService translateService, ApplicationProperties applicationProperties) {
        this.translateService = translateService;
        this.applicationProperties = applicationProperties;
    }

    @Override
    public String getGraphUri() {
        return DeeplTranslateService.URI;
    }

    /**
     * Process the incoming model
     *
     * @param model
     */
    public void process(MonitorModel model) {
        // clean model for overridden translations
        cleanModel(model);

        // collect properties to translate
        Set<Statement> statements = model.listStatements().toList().stream()
            .filter(stmt -> applicationProperties.getProperties().contains(stmt.getPredicate().getURI()))
            .filter(stmt -> stmt.getObject().isLiteral())
            .filter(stmt -> stmt.getObject().asLiteral().getLanguage().equals(SRC_LANGUAGE.toString().toLowerCase()))
            .collect(Collectors.toSet());

        // process for each language
        for (Language language : applicationProperties.getLanguages()) {
            processStatementsByLanguage(statements, language);
        }
    }

    /**
     * Clean model for overridden translations
     *
     * @param model
     */
    private void cleanModel(Model model) {
        // for each reification property
        model.listStatements(null, Datatourisme.hasTranslatedProperty, (RDFNode) null).toList()
            .forEach(stmt -> {

                // legacy behavior : thanks to Serwan LEBOULET, some resource has no properties
                if (stmt.getObject().asResource().listProperties().toList().size() == 0) {
                    model.remove(stmt);
                    return;
                }

                ReificationHelper reification = new ReificationHelper(stmt);

                // get original and translated statements
                Optional<Statement> originalStatement = reification.getOriginalStatement();
                Optional<Statement> translatedStatement = reification.getTranslatedStatement();

                // update : original or translated statement is now missing
                if (originalStatement.isEmpty() || translatedStatement.isEmpty()) {
                    model.remove(reification.getSubModel());
                    model.remove(stmt);
                    originalStatement.ifPresent(model::remove);
                    translatedStatement.ifPresent(model::remove);
                    return;
                }

                // override : find orphans statements (the producer provide his own translation)
                if (reification.listOrphanStatements().size() > 0) {
                    translatedStatement.ifPresent(model::remove);
                    model.remove(reification.getSubModel());
                    model.remove(stmt);
                }
            });
    }

    /**
     * Process list of statements for a given language
     *
     * @param statements
     * @param language
     */
    private void processStatementsByLanguage(Set<Statement> statements, Language language) {
        // only untranslated statements
        statements = statements.stream()
            .filter(stmt -> stmt.getSubject().asResource().getProperty(stmt.getPredicate(), language.toString().toLowerCase()) == null)
            .collect(Collectors.toSet());

        // determine set of untranslated unique objects
        Set<Literal> literals = statements.stream()
            .map(Statement::getObject)
            .map(RDFNode::asLiteral)
            .collect(Collectors.toSet());

        // perform translation
        List<String> texts = literals.stream()
            .map(Literal::getString)
            .filter(s -> s.contains(" ")) // avoid to translate single words
            .collect(Collectors.toList());

        if (texts.size() == 0) {
            return;
        }

        Map<String, String> translations = translateService.translate(texts, SRC_LANGUAGE, language);

        // apply translations to statements
        statements.forEach(statement -> {
            String source = statement.getObject().asLiteral().getString();
            if (translations.containsKey(source)) {
                String translation = translations.get(source);
                if (translation != null) {
                    addTranslation(statement, source, translation, language);
                }
            }
        });
    }

    /**
     * Add a translation for the given statement
     *
     * @param statement
     * @param translation
     * @param language
     */
    private void addTranslation(Statement statement, String source, String translation, Language language) {
        Resource subject = statement.getSubject();
        Property property = statement.getPredicate();
        Model model = subject.getModel();

        // add the translation
        RDFNode object = ResourceFactory.createLangLiteral(translation, language.toString().toLowerCase());
        model.add(subject, statement.getPredicate(), object);

        // add the reification property
        Resource reification = ReificationHelper.createReificationResource(model, subject, property, source, translation, language);
        model.add(subject, Datatourisme.hasTranslatedProperty, reification);
    }

    /**
     * ReificationHelper
     */
    private static class ReificationHelper {
        private final Resource subject;
        private final Resource reification;
        private final Property property;
        private final String language;
        private final String checksum;

        private Statement originalStatement;
        private Statement translatedStatement;

        public ReificationHelper(Statement stmt) {
            subject = stmt.getSubject();
            reification = stmt.getObject().asResource();
            property = reification.getProperty(RDF.predicate).getObject().as(Property.class);
            language = reification.getProperty(RDF.language).getObject().asLiteral().getString();
            checksum = reification.getProperty(DatatourismeMetadata.checksum).getObject().asLiteral().getString();

            String targetChecksum;
            if (checksum.contains("-")) {
                String sourceChecksum = checksum.split("-")[0];
                targetChecksum = checksum.split("-")[1];
                originalStatement = subject.listProperties(property, SRC_LANGUAGE.toString().toLowerCase()).toList().stream()
                    .filter(s -> DigestUtils.md5Hex(s.getObject().asLiteral().getString()).equals(sourceChecksum)).findFirst().orElse(null);
            } else {
                // legacy
                targetChecksum = checksum;
            }

            translatedStatement = stmt.getSubject().listProperties(property, language).toList().stream()
                .filter(s -> DigestUtils.md5Hex(s.getObject().asLiteral().getString()).equals(targetChecksum)).findFirst().orElse(null);

        }

        public Optional<Statement> getOriginalStatement() {
            return Optional.ofNullable(originalStatement);
        }

        public Optional<Statement> getTranslatedStatement() {
            return Optional.ofNullable(translatedStatement);
        }

        public Model getSubModel() {
            return JenaUtils.getRootedSubModel(reification);
        }

        /**
         * Helper method to find orphans statements, aka all other statements witch is not associated
         * with any other reifications
         *
         * @return
         */
        public List<Statement> listOrphanStatements() {
            Model model = subject.getModel();
            return subject.listProperties(property, language).toList().stream()
                .filter(s -> {
                    String md5 = DigestUtils.md5Hex(s.getObject().asLiteral().getString());
                    return model.listObjectsOfProperty(subject, Datatourisme.hasTranslatedProperty).toList().stream()
                        .filter(node -> node.asResource().hasProperty(DatatourismeMetadata.checksum))
                        .noneMatch(node -> {
                            String checksum = node.asResource().getProperty(DatatourismeMetadata.checksum).getObject().asLiteral().getString();
                            return checksum.endsWith("-" + md5) || checksum.equals(md5);
                        });
                }).collect(Collectors.toList());
        }

        /**
         * Create a reification property for the given propery/language,
         * unless the model already contain it.
         *
         * @param property
         * @param language
         * @return The reification resource
         */
        private static Resource createReificationResource(Model model, Resource subject, Property property, String source, String translation, Language language) {
            Resource resource = model.createResource();
            String checksum = DigestUtils.md5Hex(source) + "-" + DigestUtils.md5Hex(translation);
            resource.addProperty(RDF.predicate, property);
            resource.addProperty(RDF.language, language.toString().toLowerCase());
            resource.addProperty(DatatourismeMetadata.checksum, checksum);
            resource.addProperty(DC_11.contributor, DeeplTranslateService.URI);

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Calendar cal = Calendar.getInstance();
            resource.addProperty(DC_11.date, ResourceFactory.createTypedLiteral(dateFormat.format(cal.getTime()), XSDDatatype.XSDdate));

            String uuid = JenaUtils.resourceUUID(resource, subject);
            String uri = DatatourismeData.resource(uuid).getURI();

            return ResourceUtils.renameResource(resource, uri);
        }
    }
}
