/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.pipeline.translate;

import org.apache.jena.ext.com.google.common.collect.ImmutableList;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.concurrent.Immutable;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {Configuration.class})
@TestPropertySource("classpath:application.properties")
public class DeeplTranslateServiceTest {


    @Value( "${deepl.authkey}" )
    String authKey;

    @Autowired
    DeeplTranslateService translateService;

    @Test
    @Ignore
    public void translate() throws IOException {
        List<String> lists = ImmutableList.of("un", "deux", "trois", "quatre", "cinq");

//        DB db = DBMaker.memoryDB().make();
//        DeeplTranslateService translateService = new DeeplTranslateService(authKey, db);
        Map<String, String> result = translateService.translate(lists, Language.FR, Language.EN);
        assertEquals(lists.size(), result.size());
    }
}