/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.pipeline.translate;

import fr.datatourisme.pipeline.MessageListener;
import org.apache.jena.ext.com.google.common.io.ByteStreams;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.seaborne.patch.RDFPatch;
import org.seaborne.patch.RDFPatchOps;
import org.seaborne.patch.changes.PatchSummary;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {Configuration.class})
@TestPropertySource("classpath:application.properties")
public class TranslateMessageListenerTest {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    RabbitTemplate rabbitTemplate;

    @Mock
    DeeplTranslateService translateService;

    @Autowired
    ApplicationProperties applicationProperties;

    @Captor
    private ArgumentCaptor<Message> messageCaptor;

    @Before
    public void setUp() throws IOException {
        // stub methods
        lenient().when(translateService.translate(anyList(), same(Language.FR), any(Language.class))).thenAnswer(invocation -> invocation.getArgument(0, List.class).stream().collect(Collectors.toMap(Function.identity(), t -> new StringBuilder(t.toString()).reverse().toString())));
    }

    @Test
    public void process() throws IOException {
        PatchSummary summary = processResource("data20.nt", 1, 1);
        assertThat(summary.getCountAddData(), is(280L));
        assertThat(summary.getCountDeleteData(), is(0L));
    }

    /**
     * In this test, a new original text is provided : old translation must be deleted, a new one must be added
     *
     * @throws IOException
     */
    @Test
    public void processUpdate() throws IOException {
        PatchSummary summary = processResource("update.nt", 1, 1);
        assertThat(summary.getCountAddData(), is(7L));
        assertThat(summary.getCountDeleteData(), is(7L));
    }

    /**
     * In this test, producer provide his own translation : deepl version must be deleted
     *
     * @throws IOException
     */
    @Test
    public void processOverride() throws IOException {
        PatchSummary summary = processResource("override.nt", 0, 1);
        assertThat(summary.getCountAddData(), is(0L));
        assertThat(summary.getCountDeleteData(), is(7L));
    }

    /**
     * Test legacy checksum format behavior
     *
     * @throws IOException
     */
    @Test
    public void processLegacy() throws IOException {
        PatchSummary summary = processResource("legacy.nt", 1, 1);
        assertThat(summary.getCountAddData(), is(6L));
        assertThat(summary.getCountDeleteData(), is(6L));
    }

    /**
     * Test if another property with same label is not impacted
     *
     * @throws IOException
     */
    @Test
    public void processSameLabel() throws IOException {
        PatchSummary summary = processResource("same_label.nt", 1, 1);
        assertThat(summary.getCountAddData(), is(7L));
        assertThat(summary.getCountDeleteData(), is(0L));
    }

    /**
     * Test if 2 reification resources are created for the same properties
     *
     * @throws IOException
     */
    @Test
    public void processUnique() throws IOException {
        PatchSummary summary = processResource("unique.nt", 1, 1);
        assertThat(summary.getCountAddData(), is(14L));
        assertThat(summary.getCountDeleteData(), is(0L));
    }

    /**
     * Test if 2 reification resources are created for the same properties
     *
     * @throws IOException
     */
    @Test
    public void processNothingTodo() throws IOException {
        processResource("nothing.nt");
        verify(translateService, never()).translate(anyList(), same(Language.FR), any(Language.class));
        verify(rabbitTemplate, never()).send(anyString(), any(Message.class));
    }

    /**
     * @param file
     * @return
     */
    private PatchSummary processResource(String file, int translateNumberOfInvocations, int sendNumberOfInvocations) throws IOException {
        processResource(file);

        // verifications
        verify(translateService, times(translateNumberOfInvocations)).translate(anyList(), same(Language.FR), any(Language.class));
        verify(rabbitTemplate, times(sendNumberOfInvocations)).send(anyString(), messageCaptor.capture());
        RDFPatch rdfPatch = RDFPatchOps.read(new ByteArrayInputStream(messageCaptor.getValue().getBody()));
        System.out.println(RDFPatchOps.str(rdfPatch));

        return RDFPatchOps.summary(rdfPatch);
    }

    /**
     * @param file
     * @throws IOException
     */
    private void processResource(String file) throws IOException {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(file);
        byte[] body = ByteStreams.toByteArray(inputStream);
        // process message
        TranslateModelProcessor modelProcessor = new TranslateModelProcessor(translateService, applicationProperties);
        MessageListener messageListener = new MessageListener(rabbitTemplate, modelProcessor);
        Message message = new Message(body, new MessageProperties());
        messageListener.onMessage(message);
    }
}