# This file is part of the DATAtourisme project.
# 2022
# @author Conjecto <contact@conjecto.com>
# SPDX-License-Identifier: GPL-3.0-or-later
# For the full copyright and license information, please view the LICENSE file that was distributed with this source code.

# -----
# Build
# -----
FROM maven:3-jdk-11 AS build
WORKDIR /build

# dependencies
COPY pom.xml /build/pom.xml
COPY common/pom.xml /build/common/pom.xml
COPY translate/pom.xml /build/translate/pom.xml
COPY wikidata/pom.xml /build/wikidata/pom.xml

# common
RUN mvn -pl common dependency:go-offline
COPY common/src/. /build/common/src/
RUN mvn -pl common clean install

# modules
RUN mvn dependency:go-offline
COPY translate/src/. /build/translate/src/
COPY wikidata/src/. /build/wikidata/src/
RUN mvn package -DskipTests

# -----
# Release
# -----
FROM adoptopenjdk/openjdk11:alpine-jre AS translate
COPY --from=build /build/translate/target/*.jar /app.jar
CMD ["java", "-jar", "/app.jar"]

FROM adoptopenjdk/openjdk11:alpine-jre AS wikidata
COPY --from=build /build/wikidata/target/*.jar /app.jar
CMD ["java", "-jar", "/app.jar"]