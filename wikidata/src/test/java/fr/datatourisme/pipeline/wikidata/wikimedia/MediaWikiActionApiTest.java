/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.pipeline.wikidata.wikimedia;

import com.fasterxml.jackson.databind.JsonNode;
import fr.datatourisme.pipeline.wikidata.mediawiki.MediaWikiActionApi;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.Map;

class MediaWikiActionApiTest {
    @Test
    void query() {
        JsonNode node = MediaWikiActionApi.WIKIPEDIA.imageinfo("File:Jardin botanique du Montet - chapelle Sainte-Valérie et alpinum.jpg", "extmetadata", "url");
        Assert.assertEquals(4, node.size());
    }
}