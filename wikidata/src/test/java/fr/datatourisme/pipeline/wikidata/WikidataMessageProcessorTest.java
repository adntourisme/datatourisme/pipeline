/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.pipeline.wikidata;

import fr.datatourisme.pipeline.MessageListener;
import fr.datatourisme.pipeline.wikidata.reconciliation.ReconciliationServiceApi;
import org.apache.jena.ext.com.google.common.io.ByteStreams;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.seaborne.patch.RDFPatch;
import org.seaborne.patch.RDFPatchOps;
import org.seaborne.patch.changes.PatchSummary;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {Configuration.class})
@TestPropertySource("classpath:application.properties")
public class WikidataMessageProcessorTest {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    RabbitTemplate rabbitTemplate;

    @Captor
    private ArgumentCaptor<Message> messageCaptor;

    @Autowired
    ReconciliationHelper reconciliationHelper;

    @Test
    public void process() throws IOException {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("message/data20.nt");
        byte[] body = ByteStreams.toByteArray(inputStream);

        WikidataModelProcessor modelProcessor = new WikidataModelProcessor(reconciliationHelper);
        MessageListener messageListener = new MessageListener(rabbitTemplate, modelProcessor);
        Message message = new Message(body, new MessageProperties());
        messageListener.onMessage(message);

        // verifications
        verify(rabbitTemplate, times(1)).send(anyString(), messageCaptor.capture());
        RDFPatch rdfPatch = RDFPatchOps.read(new ByteArrayInputStream(messageCaptor.getValue().getBody()));
        PatchSummary summary = RDFPatchOps.summary(rdfPatch);
        assertThat(summary.getCountAddData(), is(89L));
        assertThat(summary.getCountDeleteData(), is(0L));
    }

    @Test
    public void processOverride() throws IOException {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("message/override.nt");
        byte[] body = ByteStreams.toByteArray(inputStream);

        WikidataModelProcessor modelProcessor = new WikidataModelProcessor(reconciliationHelper);
        MessageListener messageListener = new MessageListener(rabbitTemplate, modelProcessor);
        Message message = new Message(body, new MessageProperties());
        messageListener.onMessage(message);

        // verifications
        verify(rabbitTemplate, times(1)).send(anyString(), messageCaptor.capture());
        RDFPatch rdfPatch = RDFPatchOps.read(new ByteArrayInputStream(messageCaptor.getValue().getBody()));
        PatchSummary summary = RDFPatchOps.summary(rdfPatch);
        assertThat(summary.getCountAddData(), is(14L));
        assertThat(summary.getCountDeleteData(), is(14L));
    }
}