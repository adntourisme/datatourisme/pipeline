/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.pipeline.wikidata.utils;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class GeoUtilsTest {
    @Test
    public void distance() {
        // ex : https://data.datatourisme.gouv.fr/23/6ed160fd-8d23-377c-90c9-e2b7a496bc18 vs https://www.wikidata.org/wiki/Q810532
        double distance = GeoUtils.distance(GeoUtils.latLng(44.6950167, -1.1418914), GeoUtils.latLng(44.77, -1.15));
        Assert.assertTrue(distance < 10000);
    }
}