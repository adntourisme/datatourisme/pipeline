/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.pipeline.wikidata;

import com.fasterxml.jackson.databind.JsonNode;
import fr.datatourisme.pipeline.vocabulary.WikidataEntity;
import fr.datatourisme.pipeline.vocabulary.WikidataPropDirect;
import fr.datatourisme.pipeline.wikidata.RDFConnectionWikidata;
import fr.datatourisme.pipeline.wikidata.mediawiki.MediaWikiActionApi;
import org.apache.jena.arq.querybuilder.ConstructBuilder;
import org.apache.jena.graph.Triple;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdfconnection.*;
import org.apache.jena.sparql.core.Var;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Map;

//@RunWith(SpringRunner.class)
//@ContextConfiguration(classes = {Configuration.class})
//@TestPropertySource("classpath:application.properties")
public class WikidataStressTest {

    @Test
    @Ignore
    public void name() {


        for(int i =0; i<10000; i++) {

            int id = (int)(Math.random() * 9000000) + 1;
            Resource resource = WikidataEntity.resource("Q" + id);

            ConstructBuilder construct = new ConstructBuilder();
            Triple triple = new Triple(Var.alloc("s"), Var.alloc("p"), Var.alloc("o"));
            construct.addConstruct(triple);
            construct.addWhere(triple);
            construct.addValueVar("s", resource);

            System.out.println(resource);

            try (RDFConnection conn = RDFConnectionWikidata.create().build()) {
                Model model = conn.queryConstruct(construct.build());
                System.out.println(model.size());
//                model.listObjectsOfProperty(WikidataPropDirect.property("P18")).forEachRemaining(rdfNode -> {
//                    try {
//                        System.out.println(rdfNode.asResource().getURI());
//                        String file = rdfNode.asResource().getURI().replace("Special:FilePath/", "File:");
//                        file = file.substring(file.indexOf("File:"));
//                        file = URLDecoder.decode(file, StandardCharsets.UTF_8.toString());
//                        System.out.println(file);
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                });
            }
        }
    }
}
