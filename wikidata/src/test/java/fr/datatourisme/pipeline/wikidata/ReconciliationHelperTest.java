/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.pipeline.wikidata;

import fr.datatourisme.pipeline.vocabulary.Datatourisme;
import fr.datatourisme.pipeline.vocabulary.Schema;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.shared.PrefixMapping;
import org.apache.jena.shared.impl.PrefixMappingImpl;
import org.apache.jena.vocabulary.OWL;
import org.apache.jena.vocabulary.RDFS;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import javax.xml.crypto.Data;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilterOutputStream;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {Configuration.class})
@TestPropertySource("classpath:application.properties")
public class ReconciliationHelperTest {

    @Autowired
    ReconciliationHelper reconciliationHelper;

    @Test
    public void reconcileLegacy() {
        String data = getClass().getClassLoader().getResource("reconciliation/legacy.ttl").getPath();
        Model model = RDFDataMgr.loadModel(data);

        Map<Resource, Resource> fixtures = model.listStatements(null, OWL.sameAs, (RDFNode) null).toList().stream()
            .peek(model::remove)
            .collect(Collectors.toMap(Statement::getSubject, stmt -> stmt.getObject().asResource()));

        fixtures.forEach((res, sameAs) -> {
            System.out.println(res.getProperty(RDFS.label, "fr").getString());
            Resource output = reconciliationHelper.reconcile(res);
            assertNotNull(output);
            assertEquals(sameAs.getURI(), output.getURI());
        });
    }

    @Test
    public void reconcilePlaceOfInterest() {
        // "Bassin d'arcachon" => http://www.wikidata.org/entity/Q810532 (https://data.datatourisme.gouv.fr/23/6ed160fd-8d23-377c-90c9-e2b7a496bc18)
        // "cirque de Gavarnie" => http://www.wikidata.org/entity/Q1093112
        String data = getClass().getClassLoader().getResource("reconciliation/places.ttl").getPath();
        Model model = RDFDataMgr.loadModel(data);

        Map<Resource, Resource> fixtures = model.listStatements(null, OWL.sameAs, (RDFNode) null).toList().stream()
                .peek(model::remove)
                .collect(Collectors.toMap(Statement::getSubject, stmt -> stmt.getObject().asResource()));

        fixtures.forEach((res, sameAs) -> {
            System.out.println(res.getProperty(RDFS.label, "fr").getString());
            Resource output = reconciliationHelper.reconcile(res);
            assertNotNull(output);
            assertEquals(sameAs.getURI(), output.getURI());
        });
    }

    /*
    private void rewritePrefixedFile(Model model, String file) throws FileNotFoundException {
        PrefixMapping prefixMapping = new PrefixMappingImpl();
        prefixMapping.setNsPrefixes(PrefixMapping.Standard);
        prefixMapping.setNsPrefix(Datatourisme.PREFIX, Datatourisme.getURI());
        prefixMapping.setNsPrefix(Schema.PREFIX, Schema.getURI());
        model.setNsPrefixes(prefixMapping);
        RDFDataMgr.write(new FileOutputStream(new File(file)), model, Lang.TTL);
    }
    */

    /*
    PREFIX dt: <https://www.datatourisme.gouv.fr/ontology/core#>
    PREFIX schema: <http://schema.org/>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

    CONSTRUCT {
      ?s rdfs:label ?label.
      ?s rdf:type ?type.
      ?s dt:isLocatedAt ?isLocatedAt.
      ?isLocatedAt schema:geo ?geo.
      ?isLocatedAt schema:address ?address.
      ?geo ?p0 ?o0.
      ?address ?p1 ?o1
    } WHERE {
      ?s rdfs:label ?label.
      ?s rdf:type ?type.
      ?s dt:isLocatedAt ?isLocatedAt.
      ?isLocatedAt schema:geo ?geo.
      ?isLocatedAt schema:address ?address.
      ?geo ?p0 ?o0.
      ?address ?p1 ?o1
    } VALUES ?s { <https://data.datatourisme.gouv.fr/23/6ed160fd-8d23-377c-90c9-e2b7a496bc18> }


    ?s owl:sameAs ?sameAs
    */
}