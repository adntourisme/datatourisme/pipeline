/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.pipeline.wikidata.reconciliation;

import fr.datatourisme.pipeline.wikidata.Configuration;
import org.apache.jena.ext.com.google.common.io.ByteStreams;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.io.InputStream;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {Configuration.class})
@TestPropertySource("classpath:application.properties")
public class ReconciliationServiceApiTest {

    @Autowired
    ReconciliationServiceApi reconciliationServiceApi;

    @Test
    public void test() throws IOException {
        ReconciliationQuery query = ReconciliationQuery.builder()
            .setQuery("eglise saint genest")
            .addType("Q2221906")
            .addProperty("P17", "Q142")
            .addProperty("P131/P374", 41113)
            .setLimit(1)
            .build();

        Reconciliation reconciliation = reconciliationServiceApi.execute(query);
        Assert.assertEquals(reconciliation.getBestCandidate().id, "Q3581522");
    }
}