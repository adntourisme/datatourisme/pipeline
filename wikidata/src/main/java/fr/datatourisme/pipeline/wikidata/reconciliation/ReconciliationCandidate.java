/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.pipeline.wikidata.reconciliation;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.Collections;
import java.util.List;

public class ReconciliationCandidate {
    @JsonProperty("name")
    public String name;

    @JsonProperty("id")
    public String id;

    @JsonProperty("type")
    public List<Type> types = Collections.emptyList();

    @JsonProperty("features")
    public List<Feature> features = Collections.emptyList();

    @JsonProperty("score")
    public double score;

    @JsonProperty("match")
    public boolean match = false;

    public Feature getFeature(String id) {
        return features.stream().filter(feature -> feature.id.equals(id)).findFirst().orElse(null);
    }

    @Override
    public String toString() {
        try {
            return ParsingUtilities.mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            return super.toString();
        }
    }

    /**
     * Type
     */
    public static class Type {
        @JsonProperty("id")
        public String id;
        @JsonProperty("name")
        public String name;

        @JsonCreator
        public Type(@JsonProperty("id") String id, @JsonProperty("name") String name) {
            this.id = id;
            this.name = name;
        }

        @JsonCreator
        public Type(String id) {
            this.id = id;
            this.name = null;
        }
    }

    /**
     * Feature
     */
    public static class Feature {
        @JsonProperty("id")
        public String id;
        @JsonProperty("value")
        public Object value;

        @JsonCreator
        public Feature(@JsonProperty("id") String id, @JsonProperty("value") Object value) {
            this.id = id;
            this.value = value;
        }
    }
}
