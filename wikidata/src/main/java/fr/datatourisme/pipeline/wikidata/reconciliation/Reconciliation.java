/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.pipeline.wikidata.reconciliation;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.*;
import java.util.stream.Collectors;

public class Reconciliation {
    @JsonIgnore
    private List<ReconciliationCandidate> candidates;

    @JsonProperty("candidates")
    public List<ReconciliationCandidate> getCandidates() {
        if (candidates != null) {
            return candidates;
        }
        return Collections.emptyList();
    }

    public void addCandidate(ReconciliationCandidate candidate) {
        if (candidates == null) {
            candidates = new ArrayList<>();
        }
        candidates.add(candidate);
    }

    @JsonIgnore
    /**
     * Return the best candidate
     */
    public ReconciliationCandidate getBestCandidate() {
        if (candidates != null && candidates.size() > 0) {
            return candidates.get(0);
        }
        return null;
    }

    @JsonIgnore
    /**
     * Return the best candidate witch perfectly match given features
     */
    public ReconciliationCandidate getBestMatchCandidate(String... featureIds) {
        if (candidates != null) {
            return candidates.stream().filter(c -> {
                Map<String, Integer> features = c.features.stream().collect(Collectors.toMap(f -> f.id, f -> Integer.parseInt(f.value.toString())));
                for (String fid : featureIds) {
                    if (features.getOrDefault(fid, 0) != 100) {
                        return false;
                    }
                }
                return true;
            }).findFirst().orElse(null);
        }
        return null;
    }

    @Override
    public String toString() {
        try {
            return ParsingUtilities.mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            return super.toString();
        }
    }
}
