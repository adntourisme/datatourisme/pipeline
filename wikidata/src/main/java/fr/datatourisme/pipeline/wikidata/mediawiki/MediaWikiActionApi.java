/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.pipeline.wikidata.mediawiki;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.jena.ext.com.google.common.collect.ImmutableList;
import org.apache.jena.ext.com.google.common.collect.ImmutableMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.*;

/**
 * A Simple Mediawiki Action API wrapper for fr.wikipedia.org
 */
public class MediaWikiActionApi {
    private static final Logger LOG = LoggerFactory.getLogger(MediaWikiActionApi.class);

    private final String endpointUrl;
    private final CloseableHttpClient httpClient;

    public final static MediaWikiActionApi WIKIPEDIA = new MediaWikiActionApi("https://en.wikipedia.org/w/api.php");
    public final static MediaWikiActionApi WIKIDATA = new MediaWikiActionApi("https://www.wikidata.org/w/api.php");
    public final static MediaWikiActionApi WIKIDATA_TEST = new MediaWikiActionApi("https://test.wikidata.org/w/api.php");
    public final static MediaWikiActionApi WIKIMEDIA_COMMONS = new MediaWikiActionApi("https://commons.wikimedia.org/w/api.php");

    public MediaWikiActionApi(String endpointUrl) {
        this.endpointUrl = endpointUrl;

        RequestConfig defaultRequestConfig = RequestConfig.custom()
            .setConnectTimeout(30 * 1000)
            .setSocketTimeout(60 * 1000)
            .build();

        this.httpClient = HttpClientBuilder.create()
            .setDefaultRequestConfig(defaultRequestConfig)
            .build();
    }

    /**
     * Get infos about image
     *
     * @param titles
     * @param iiprops
     */
    public JsonNode imageinfo(String titles, String... iiprops) {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("iiextmetadatalanguage", "fr");
        JsonNode node = query(titles, ImmutableList.of("imageinfo"), ImmutableMap.of("iiprop", Arrays.asList(iiprops)), parameters);
        if (node != null && node.has("imageinfo")) {
            return node.get("imageinfo").get(0);
        }

        return null;
    }

    /**
     * Query action
     *
     * @param titles
     * @param props
     */
    public JsonNode query(String titles, List<String> props, Map<String, List<String>> additionalProps, Map<String, String> additionalParameters) {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("action", "query");
        parameters.put("titles", titles);
        parameters.put("prop", String.join("|", props));
        if (additionalProps != null) {
            additionalProps.forEach((k,v) -> {
                parameters.put(k, String.join("|", v));
            });
        }
        if (additionalParameters != null) {
            parameters.putAll(additionalParameters);
        }

        JsonNode root = sendJsonRequest(parameters);
        if (root != null) {
            JsonNode pages = root.path("query").path("pages");
            if (!pages.isMissingNode()) {
                if (pages.iterator().hasNext()) {
                    return pages.iterator().next();
                }
            }
        }

        return null;
    }

    /**
     * @param parameters
     * @return
     */
    private JsonNode sendJsonRequest(Map<String, String> parameters) {
        parameters.put("format", "json");
        try {
            URIBuilder builder = new URIBuilder(endpointUrl);
            parameters.forEach(builder::setParameter);

            HttpGet request = new HttpGet(builder.build());
            CloseableHttpResponse response = httpClient.execute(request);
            try {
                StatusLine statusLine = response.getStatusLine();
                if(statusLine.getStatusCode() != 200) {
                    throw new HttpResponseException(statusLine.getStatusCode(), statusLine.getReasonPhrase());
                }

                HttpEntity entity = response.getEntity();
                String result = EntityUtils.toString(entity);
                ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

                return mapper.readTree(result);
            } finally {
                response.close();
            }
        } catch (URISyntaxException | IOException e) {
            LOG.error(e.getMessage());
        }

        return null;
    }
}
