/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.pipeline.utils;

import org.apache.jena.rdf.model.*;
import java.util.UUID;

public class JenaUtils {
    /**
     * Generate a resource UUID base on properties
     * You can use ResourceUtils.renameResource to rename an anonymous resource
     *
     * @param resource
     * @return
     */
    static public String resourceUUID(Resource resource, Resource subject) {
        String identity = subject != null ? subject.getURI() : "";
        String hash = resource.listProperties().toList().stream().map(stmt -> {
            return UUID.nameUUIDFromBytes((stmt.getPredicate().getURI() + "|" + stmt.getObject().toString()).getBytes()).toString();
        }).sorted().reduce(identity, (a, b) -> {
            return a + b;
        });

        if (hash.equals("")) {
            hash = resource.getURI();
        }

        return UUID.nameUUIDFromBytes(hash.getBytes()).toString();
    }

    static public String resourceUUID(Resource resource) {
        return resourceUUID(resource, null);
    }

    /**
     * Try to follow the properties sequence from the given resource to get
     * the final value.
     *
     * @return RDFNode, null if the sequence cannot be followed
     */
    static public RDFNode getPropertySequenceValue(Resource resource, Property... properties) {
        RDFNode res = resource;
        for (Property property : properties) {
            if (res.isResource() && res.asResource().hasProperty(property)) {
                res = res.asResource().getProperty(property).getObject();
            } else {
                return null;
            }
        }

        return res;
    }

    /**
     * Return a reduced model with a resource and all related sub-resources
     */
    static public Model getRootedSubModel(Resource resource)  {
        ModelExtract modelExtract = new ModelExtract(new StatementTripleBoundary(t -> false));
        return modelExtract.extract(resource, resource.getModel());
    }
}
