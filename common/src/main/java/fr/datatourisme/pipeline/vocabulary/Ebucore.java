/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.pipeline.vocabulary;

import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;

public class Ebucore {

    public static final Resource resource(String suffix )
    { return ResourceFactory.createResource( NS + suffix ); }

    public static final Property property(String suffix )
    { return ResourceFactory.createProperty( NS + suffix ); }

    /** <p>The namespace of the vocabulary as a string ({@value})</p> */
    public static final String NS = "http://www.ebu.ch/metadata/ontologies/ebucore/ebucore#";

    /** <p>The namespace of the vocabulary as a string</p>
     *  @see #NS */
    public static String getURI() {return NS;}

    public static final String PREFIX = "ebucore";

    /** <p>The namespace of the vocabulary as a resource</p> */
    public static final Resource NAMESPACE = resource( NS );

    // Vocabulary classes
    ///////////////////////////
    public static final Resource EditorialObject = resource( "EditorialObject" );
    public static final Resource Resource = resource( "Resource" );
    public static final Resource Annotation = resource( "Annotation" );

    // Vocabulary properties
    ///////////////////////////
    public static final Property hasRelatedResource = property( "hasRelatedResource" );
    public static final Property hasAnnotation = property( "hasAnnotation" );

    public static final Property locator = property( "locator" );
    public static final Property hasMimeType = property( "hasMimeType" );
    public static final Property fileSize = property( "fileSize" );
    public static final Property width = property( "width" );
    public static final Property height = property( "height" );

    public static final Property isCoveredBy = property( "isCoveredBy" );
    public static final Property title = property( "title" );
}
