/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.pipeline.vocabulary;

import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;

public class Datatourisme {

    public static final Resource resource(String suffix )
    { return ResourceFactory.createResource( NS + suffix ); }

    public static final Property property(String suffix )
    { return ResourceFactory.createProperty( NS + suffix ); }

    /** <p>The namespace of the vocabulary as a string ({@value})</p> */
    public static final String NS = "https://www.datatourisme.gouv.fr/ontology/core#";

    /** <p>The namespace of the vocabulary as a string</p>
     *  @see #NS */
    public static String getURI() {return NS;}

    public static final String PREFIX = "dt";

    /** <p>The namespace of the vocabulary as a resource</p> */
    public static final Resource NAMESPACE = resource( NS );

    // Vocabulary classes
    ///////////////////////////
    public static final Resource PointOfInterest = resource( "PointOfInterest" );

    public static final Resource PlaceOfInterest = resource( "PlaceOfInterest" );
    public static final Resource EntertainmentAndEvent = resource( "EntertainmentAndEvent" );
    public static final Resource Product = resource( "Product" );
    public static final Resource Tour = resource( "Tour" );

    public static final Resource CulturalSite = resource( "CulturalSite" );
    public static final Resource NaturalHeritage = resource( "NaturalHeritage" );

    // Vocabulary properties
    ///////////////////////////
    public static final Property hasTranslatedProperty = property( "hasTranslatedProperty" );
    public static final Property hasAddressCity = property( "hasAddressCity" );
    public static final Property isLocatedAt = property( "isLocatedAt" );
    public static final Property hasRepresentation = property( "hasRepresentation" );
    public static final Property credits = property( "credits" );

}
