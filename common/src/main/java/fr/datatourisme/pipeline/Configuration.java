/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.pipeline;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;

@org.springframework.context.annotation.Configuration
public class Configuration {

    @Value( "${batch.size:1}" )
    protected int batchSize;

    @Bean
    public ConnectionFactory connectionFactory(@Value( "${rabbitmq.host}" ) String host, @Value( "${rabbitmq.port:5672}" ) int port, @Value( "${rabbitmq.username:#{null}}" ) String username, @Value( "${rabbitmq.password:#{null}}" ) String password) {
        CachingConnectionFactory factory = new CachingConnectionFactory(host, port);
        if (username != null) {
            factory.setUsername(username);
        }
        if (password != null) {
            factory.setPassword(password);
        }
        return factory;
    }

    @Bean
    public AmqpAdmin amqpAdmin(ConnectionFactory connectionFactory) {
        return new RabbitAdmin(connectionFactory);
    }

    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        return new RabbitTemplate(connectionFactory);
    }

    @Bean
    Queue sourceQueue() {
        return QueueBuilder.nonDurable().exclusive().autoDelete().build();
    }

    @Bean
    Binding sourceBinding(Queue queue) {
        FanoutExchange fanoutExchange = ExchangeBuilder.fanoutExchange("datatourisme.pipeline.source").durable(true).build();
        return BindingBuilder.bind(queue).to(fanoutExchange);
    }

    @Bean
    MessageListener messageListener(RabbitTemplate rabbitTemplate, ModelProcessor messageProcessor) {
        return new MessageListener(rabbitTemplate, messageProcessor);
    }

    @Bean
    SimpleMessageListenerContainer listenerContainer(ConnectionFactory connectionFactory, Queue queue, MessageListener messageListener) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setMessageListener(messageListener);
        container.setQueues(queue);
        container.setDefaultRequeueRejected(false);
        container.setAutoStartup(false);
        if (batchSize > 1) {
            container.setConsumerBatchEnabled(true);
            container.setBatchSize(batchSize);
        }
        return container;
    }
}
