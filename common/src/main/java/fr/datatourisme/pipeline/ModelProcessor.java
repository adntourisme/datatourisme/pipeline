/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.pipeline;
import org.apache.jena.util.MonitorModel;

/**
 * Message processor interface
 */
public interface ModelProcessor {

    public void process(MonitorModel model);

    public String getGraphUri();
}
